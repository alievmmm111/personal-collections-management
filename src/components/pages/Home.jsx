import React, {useState} from 'react';
import { useTranslation } from 'react-i18next';
import CollectionsList from '../collections/CollectionsList';

export const Home = () => {
  const { t } = useTranslation('global');
  const [collections, setCollections] = useState([]);

  return (
    <div className="container">
      <h2 className='title pt-5 has-text-centered'>{t('home.body')}</h2>
      <CollectionsList collections={collections}/>
      </div>
  );
};
