import { createUserWithEmailAndPassword, updateProfile, signInWithPopup } from 'firebase/auth';
import React, {useState} from 'react';
import { Link, useNavigate } from "react-router-dom";
import {auth, googleProvider, twitterProvider} from '../../firebase'
import toast, { Toaster } from 'react-hot-toast';
import Icon from '@mdi/react';
import { mdiTextAccount, mdiEmailMinusOutline, mdiLockMinusOutline } from '@mdi/js';

export const SignUp = () => {
    const navigate = useNavigate();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [name, setName] = useState('');
    

    const signUp = (e) => {
        e.preventDefault();
        createUserWithEmailAndPassword(auth, email, password)
        .then(async (res) => {
            const user = res.user;
            await updateProfile(user, {
                displayName: name,
            });
            toast.success('Successfully created!');
            navigate("/");
        }).catch((error) => {
            console.log(error);
            toast.error('Email exists!');
        })
    }

    const signUpWithGoogle = (e) => {
        e.preventDefault();
        signInWithPopup(auth, googleProvider)
        .then(async () => {
            toast.success('Successfully created!');
            navigate("/");
        }).catch((error) => {
            console.error(error)
            toast.error('Error')
        })
    }

    const signUpWithTwitter = (e) => {
        e.preventDefault();
        signInWithPopup(auth, twitterProvider)
        .then(async () => {
            toast.success('Successfully created!');
            navigate("/");
        }).catch((error) => {
            console.error(error)
            toast.error('Error')
        })
    }
  return (
    <div className='container is-max-desktop '>
        <div className="container-mid">
        <form className='form' onSubmit={signUp}>
        <h1 className='title is-3'>Create an account</h1>

        <div className="field">
            <label className="label">Name</label>
            <div className="control has-icons-left">
            <input className='input' type="name" placeholder='Enter your name' value={name} onChange={(e) => setName(e.target.value)}/>
            <span class="icon is-small is-left">
            <Icon path={mdiTextAccount} size={1} />
            </span>
            </div>
        </div>

        <div className="field">
            <label className="label">Email address</label>
            <div className="control has-icons-left">
            <input className='input' type="email" placeholder='Enter your email' value={email} onChange={(e) => setEmail(e.target.value)}/>
            <span class="icon is-small is-left">
            <Icon path={mdiEmailMinusOutline} size={1} />
            </span>
            </div>
        </div>
        <div className="field">
        <label className="label">Password</label>
            <div className="control has-icons-left">
            <input className='input' type="password" placeholder='Enter password' value={password} onChange={(e) => setPassword(e.target.value)}/>
            <span class="icon is-small is-left">
            <Icon path={mdiLockMinusOutline} size={1} />
            </span>
            </div>
        </div>

        <div className="field">
            <div className="control">
            <button type='submit' className='button is-black'>Register</button>
        <Toaster />
            </div>

        </div>
        <button className='button ' onClick={signUpWithGoogle}> Continue with Google &nbsp;
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsTAAALEwEAmpwYAAACjklEQVR4nGNgGDHgfxWLxf90pk3/oxhf/vdj/PHfk/HP/wDG7/9jmF78z2Fa/b+WxZR8w2tYtf8nMN34b8fw/78tHuzA8P9/CtPl/zVsqqRZUMKU+t+d8S9ew23RsDfj7/+VTFHEWVDKlPLfiQTDbaHYl/HX/woWc8IW1LNpYfUBKEgSmW78z2Sc+7+IqeJ/IeOU/0lM1+GOIdYCsCV5TGcwLAhjfPO/hNUAq/piVp3/iUw3ibfgMIfc/73sv/9XMf+HR3Y44+v/MQzcRBlADPh/kK3g/0H2/2A8j/X/fz/Gv/8LWEwYqAn+H2DfArcEhHezzcGmrmJyTXNU94xr+HBM17Sr+ZNaF2Gz5CKKJQdYsSbHtL6+A5Jll/4TwtaNm75jWnKQ7QmKJQfZ3SixRK92/x/CPjnIGk2JJVaNW7D45AD7ZmRLbu4VXUFKnOjX7vuDbIlP2+IXWCxhywMZ/vcgx/+lO7T/B23w/d+7zcyCgQjQOqvAVq78LIpPUvv712FacpBD9v1+3l/VW2z/+6wPAOOCzQ5vOjdY8eKzYPLCWGH31hXvkC2QLT//v3FqBfbSuWWL5UWYBTCcv9Hx7eStpljzy6odWmbZ02ueocdHSMfc6zhdtXCXoWbMRs8/6BYFrPf7X7vZ+uakLSbTp2/VL5uw1Xha3WbrmyHrff/7bAj4Hzu1GW6Bbs2BP5XT67Xwhu+0bYZJIRt8UCwhBifPLfyvVnX8X/nUmjgGYsCkrcZx2HzkgwfHb/D4M3mdQzwDKWD+DmONms22V/3X++M1PGC93//6zdaXlm42IK1mRAbzthmYtm+xWF+62f5F8kbXH7EbPP+kb3T9XrbJ4XnvVrNVC3bqGpNt+JADALcBhFvzrmmCAAAAAElFTkSuQmCC"></img>
        </button>
        <p className='control mt-1'>
        <button className='button ' onClick={signUpWithTwitter}> Continue with Twitter &nbsp;
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAsTAAALEwEAmpwYAAABy0lEQVR4nO2WPU9VQRCGx52jSDTEQisba2vBCmNBAdFEEyyMjTaE8BuAQiKJFaEzYEEics/sWFDx0dlrYukPMBZASDA7syEQYc16r0T5uGfvObeQ5L7JdLPvszNnJnsAOurov9a7cAVZBw3J06zm70AIF0r5ZOR7U3ON1REk3UGr4U8YK1+A3e0YSDKF5B8XO7G/iVb2DeuLBOjzv4H/BMnPxiVWgUM3cLjU1AxJBuqH5cCwjsPHkJ2aOBcuIunWmeC6hxorK0jyprD9Wc3fPd62jH3fiTz2fc2hR5HDQrhc1L16JVY3jhsYK58Nu7H4KX53Jpf7xVDRlgbNsDxDksMmpvFiXxPA35KhGbl7hvRDbHFiK88MY+VTMhjY3YhTXRXaqPhtOhjiZOtsW8DkhlsCw2roQqvvq0H1Byxu97QGhsZakc6gld1yYJmGMsJchypUuw1L7jqUlWEdL1ir0yo9xNw/gqrKyPciyXzcyUTwa2iLlneuIclLJNkr3ludLAfhgFnu+pH1gSE/iqQ1JJWEKr8juyfVKsx3b8XFR1KXMESbSPIKePMqtPWvIteH0RhJl4yVdUOyhqSLhnQiPhLxQWkfsKOOzrt+AZB410LUq5ByAAAAAElFTkSuQmCC"></img>
        </button>
        </p>
       
        
        <p>
            Already have an account?{" "}
            <span>
              <Link to="/signin">Sign in</Link>
            </span>
          </p>
        
        </form>
    </div>
    </div>
  )
}
