import { useState, useEffect } from 'react';
import { onAuthStateChanged, signOut } from 'firebase/auth';
import { useNavigate } from 'react-router-dom';
import { auth } from '../../firebase';

export const useAuth = () => {
  const [authUser, setAuthUser] = useState(null);
  const [userName, setUserName] = useState("");
  const navigate = useNavigate();

  const handleAuthChange = (user) => {
    if (user) {
      setAuthUser(user);
      setUserName(user.displayName);
    } else {
      setAuthUser(null);
    }
  };

  const userSignOut = () => {
    signOut(auth)
      .then(() => {
        console.log('sign out successful');
        navigate("/");
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, handleAuthChange);
    return () => {
      unsubscribe();
    };
  }, []);

  return { authUser, userName, userSignOut };
};