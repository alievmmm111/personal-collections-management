import React, {useState} from 'react';
import Icon from '@mdi/react';
import { mdiLightbulbOnOutline} from '@mdi/js';


const ToggleSwitch = () => {
      const [toggle, setToggle] = useState(false);
      const handleToggleChange = () => {
        setToggle(!toggle);
      };
  
    return (
        <div className='toggle-container' onClick={handleToggleChange}>
      <div className={`toggle-btn ${!toggle ? "disable" : ""}`}>
        {toggle ? <><span><Icon path={mdiLightbulbOnOutline} size={1} /></span></> : <><span><Icon path={mdiLightbulbOnOutline} size={1} /></span></>}
      </div>
    </div>
    );
}

export default ToggleSwitch;
