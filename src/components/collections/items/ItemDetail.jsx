import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { doc, getDoc } from 'firebase/firestore';
import { db } from '../../../firebase';
import Breadcrumb  from '../../navigation/Breadcrumb';

export const ItemDetail = () => {
  const { collectionId, id } = useParams();
  const [item, setItem] = useState(null);

  useEffect(() => {
    const fetchItem = async () => {
      try {
        const itemDocRef = doc(db, 'collections', collectionId, 'items', id);
        const itemDocSnapshot = await getDoc(itemDocRef);

        if (itemDocSnapshot.exists()) {
          setItem(itemDocSnapshot.data());
        } else {
          console.error('Item not found');
        }
      } catch (error) {
        console.error('Error fetching item:', error);
      }
    };

    fetchItem();
  }, [collectionId, id]);

  if (!item) {
    return <div>Loading...</div>;
  }

  const renderBooleanValue = (value) => (value ? 'Yes' : 'No');

  return (
    <div className='section'>
      <Breadcrumb/>
      <h2 className='title'>{item.name}</h2>
      <p className='subtitle'>{item.description}</p>
      {item.additionalFields && item.additionalFields.length > 0 && item.additionalFields.map((field, index) => (
        <p key={index} className='subtitle'>
          <span className='has-text-weight-bold'>{field.name}: </span>
          {field.type === 'Boolean' ? renderBooleanValue(field.value) : field.value}
        </p>
      ))}
      <p>
        <span className='has-text-weight-bold'>Tags: </span>{item.tags ? item.tags.join(', ') : ''}
      </p>
      <p>
        <span className='has-text-weight-bold'>Author: </span> {item.author}
      </p>
    </div>
  );
};

export default ItemDetail;
