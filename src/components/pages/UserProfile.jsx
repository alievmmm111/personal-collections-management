import React, { useState, useEffect } from 'react';
import { onAuthStateChanged } from 'firebase/auth';
import {auth, db} from '../../firebase'
import {collection, query, where, getDocs } from 'firebase/firestore';
import CollectionItem from '../collections/CollectionItem'; 

export const UserProfile = ({ user }) => {
  const [authUser, setAuthUser] = useState(null);
  const [userName, setUserName] = useState("");
  const [userEmail, setUserEmail] = useState("");
  const [userCollections, setUserCollections] = useState([]);

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const user = auth.currentUser;

        if (user) {
          setAuthUser(user);
          setUserName(user.displayName);
          setUserEmail(user.email);

          const collectionsRef = collection(db, 'collections');
          const q = query(collectionsRef, where('createdBy', '==', user.displayName));
          const querySnapshot = await getDocs(q);

          const collectionsData = querySnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
          setUserCollections(collectionsData);
        } else {
          setAuthUser(null);
        }
      } catch (error) {
        console.error('Error fetching user data: ', error);
      }
    };

    const unsubscribe = onAuthStateChanged(auth, (user) => {
      fetchUserData();
    });

    return () => {
      unsubscribe();
    };
  }, []);

  return (
    <div>
      <h1 className='title'>{userName}</h1>
      <h1 className='subtitle'>{userEmail}</h1>
      <h2>{userName} Collections:</h2>
      <div className='card-collections'>        
          {userCollections.map(collection => (
                <div key={collection.id} >
                <CollectionItem collection={collection} />
                </div>
          ))}
      </div>
    </div>
  );
};
