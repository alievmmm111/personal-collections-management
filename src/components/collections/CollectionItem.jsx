import React from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

const CollectionItem = ({ collection }) => {
  const { name, createdBy, id, template } = collection;
  const {t} = useTranslation("global");

  return (
    <div>
      <div className='card card-collections'>
        <div className="card-collection-content">
          <img src={template} alt={name} className='card-collection-img' />
          <h3 className='card-item-title'>
            {name} <span className="tag is-black">{t("collection.collection")} </span>
          </h3>
          <p>{t("collection.created-by")}{createdBy}</p>
          <Link to={`/collections/${id}`}>{t("collection.view-items")}  ⟶</Link>
        </div>
      </div>
    </div>
  );
};

export default CollectionItem;
