import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Icon from '@mdi/react';
import { mdiLogin, mdiLogout, mdiMenuDown } from '@mdi/js';
import { mdilTabPlus } from '@mdi/light-js';
import { useTranslation } from 'react-i18next';
import { useAuth } from '../utils/authUtils';

const UserAuthNavPart = () => {
  const { authUser, userName, userSignOut } = useAuth(); 
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);
  const {t} = useTranslation("global");

  const toggleDropdown = () => {
    setIsDropdownOpen(!isDropdownOpen);
  };

  return (
    <div>
      {authUser ? (
        <div className="control">
          <div className={`dropdown ${isDropdownOpen ? 'is-active' : ''}`}>
            <div className="dropdown-trigger">
              <button
                className="button is-small mr-1"
                aria-haspopup="true"
                aria-controls="dropdown-menu2"
                onClick={toggleDropdown}
              >
                <span>{`${userName} `}</span>
                <span className="icon is-small">
                  <Icon path={mdiMenuDown} size={1} />
                </span>
              </button>
            </div>
            <div className="dropdown-menu" id="dropdown-menu2" role="menu">
              <div className="dropdown-content">
                <Link to="/user-details" className="dropdown-item is-small is-size-7">
                {t("navbar.my-profile")} 
                </Link>
                <div className="dropdown-item">
                  <button onClick={userSignOut} className="button is-small">
                  {t("navbar.signout")} &nbsp;<Icon path={mdiLogout} size={0.7} />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <Link to='/create-collection' className="button is-small is-black mr-1">
          {t("navbar.add-collection")} &nbsp; <Icon path={mdilTabPlus} size={1} />
          </Link>
        </div>
      ) : (
        <div className="control">
          <Link className="button is-small is-outlined" to="/signin">
            Sign In &nbsp;<Icon path={mdiLogin} size={1} />
          </Link>
        </div>
      )}
    </div>
  );
};

export default UserAuthNavPart;
