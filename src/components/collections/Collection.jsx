import React, { useState, useEffect } from 'react';
import { useParams} from 'react-router-dom';
import { getDocs, collection } from 'firebase/firestore';
import { db } from '../../firebase';
import ItemList from './items/ItemList';
import CreateItem from './items/CreateItem';

const Collection = () => {
  const { collectionId } = useParams();
  const [items, setItems] = useState([]);

  useEffect(() => {
    const fetchItems = async () => {
      try {
        const itemsSnapshot = await getDocs(collection(db, 'collections', collectionId, 'items'));
        const itemsData = itemsSnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
        setItems(itemsData);
      } catch (error) {
        console.error('Error fetching items: ', error);
      }
    };

    fetchItems();
  }, [collectionId]);

  const handleItemCreated = (newItem) => {
    console.log('Item created:', newItem);
    setItems((prevItems) => [...prevItems, newItem]);
  };

  return (
    <div>
      <CreateItem collectionId={collectionId} onItemCreated={handleItemCreated} />
      <ItemList items={items} />
    </div>
  );
};

export default Collection;
