import React from 'react';
import Icon from '@mdi/react';
import { mdiUndoVariant } from '@mdi/js';
import { mdiTag } from '@mdi/js';
import { Link, useParams } from 'react-router-dom';


export const ItemList = ({ items}) => {
  const { collectionId } = useParams();
  return (
    <div className="card-items">
      {items.map((item) => (
        <div className="card p-3" key={item.id}>
          <header className="card-header">
            <p className="card-header-title">
              {item.name} &nbsp;
              <span className="tag is-dark">Name</span>
            </p>
            <button className="card-header-icon" aria-label="more options">
              <span className="icon">
                <Icon path={mdiUndoVariant} size={1} />
              </span>
            </button>
          </header>
          <div className="card-item-content">
            <p>{item.description}</p>
            <br />
            <p>
              <span>
                <span className="tag is-black">
                  {' '}
                  <Icon style={{ verticalAlign: 'sub' }} path={mdiTag} size={0.7} />
                  Tags
                </span>
                &nbsp;
              </span>
              {item.tags ? item.tags.join(', ') : ''}
            </p>
            <p>
              <span className="tag is-black">Author</span> {item.author}
            </p>
            <Link to={`/collections/${collectionId}/items/${item.id}`}>more</Link>
          </div>
        </div>
      ))}
    </div>
  );
};

export default ItemList;
