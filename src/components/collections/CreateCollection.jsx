import React, { useState, useEffect } from 'react';
import { addDoc, collection, serverTimestamp } from 'firebase/firestore';
import { auth, db } from '../../firebase';
import booksImage from '../../static/haze-stack-of-books.png';
import stampsImage from '../../static/haze-diploma-of-a-graduate.png';
import alcoholImage from '../../static/haze-can-of-soda.png';
import flowerImage from '../../static/haze-flowers.png';
import softwareImage from '../../static/haze-code.png';
import carImage from '../../static/haze-car.png';
import medicalImage from '../../static/haze-pills.png';
import othersImage from '../../static/haze-shoe-box.png';
import toast, { Toaster } from 'react-hot-toast';

const collectionTemplates = [
  { name: 'Other', image: othersImage },
  { name: 'Books', image: booksImage },
  { name: 'Stamps', image: stampsImage },
  { name: 'Alcohol', image: alcoholImage },
  { name: 'Flowers', image: flowerImage },
  { name: 'Software', image: softwareImage },
  { name: 'Cars', image: carImage },
  { name: 'Medicals', image: medicalImage },
];

const CreateCollection = ({ onCollectionCreated }) => {
  const [newCollectionName, setNewCollectionName] = useState('');
  const [selectedTemplate, setSelectedTemplate] = useState(othersImage); 

  const createCollection = async () => {
    try {
      if (newCollectionName.trim() === '') {
        toast.error('Collection name cannot be empty !');
        return;
      }

      const newCollectionRef = await addDoc(collection(db, 'collections'), {
        name: newCollectionName,
        template: selectedTemplate,
        createdAt: serverTimestamp(),
        createdBy: auth.currentUser.displayName,
      });

      onCollectionCreated({ id: newCollectionRef.id, name: newCollectionName });
      setNewCollectionName('');
      setSelectedTemplate(othersImage);
      toast.success('Collection created successfully!');
    } catch (error) {
      console.error('Error creating collection: ', error);
    }
  };

  useEffect(() => {
    console.log(selectedTemplate);
  }, [selectedTemplate]);

  return (
    <div>
      <label>Create New Collection:</label>
      <input
        className="input"
        type="text"
        value={newCollectionName}
        onChange={(e) => setNewCollectionName(e.target.value)}
      />

      <label>Select Template:</label>
      <select
        className="select"
        value={selectedTemplate}
        onChange={(e) => setSelectedTemplate(e.target.value)}
      >
        {collectionTemplates.map((template, index) => (
          <option key={index} value={template.image}>
            {template.name}
          </option>
        ))}
      </select>

      <button className="button" onClick={createCollection}>
        Create Collection
      </button>
      <Toaster/>
    </div>
  );
};

export default CreateCollection;
