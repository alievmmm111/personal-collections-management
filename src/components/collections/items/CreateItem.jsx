import React, { useState } from 'react';
import { addDoc, collection, serverTimestamp } from 'firebase/firestore';
import { auth, db } from '../../../firebase';
import { Link } from 'react-router-dom';
import toast, { Toaster } from 'react-hot-toast';
import { handleKeyDown, removeTag, addField, removeField, updateFieldValue, updateFieldName } from '../../utils/itemFunctions';

const CreateItem = ({ collectionId, onItemCreated }) => {
  const [itemInfo, setItemInfo] = useState({
    name: '',
    description: '',
    tags: [],
    inputValue: '',
    additionalFields: [],
    newFieldName: '',
    newFieldType: '',
  });

  const createItem = async () => {
    try {
      if (!auth.currentUser) {
        toast.error('User not authenticated. Please sign in.');
        return;
      }

      const itemsCollectionRef = collection(db, 'collections', collectionId, 'items');
      const newItemDocRef = await addDoc(itemsCollectionRef, {
        name: itemInfo.name,
        description: itemInfo.description,
        tags: itemInfo.tags,
        additionalFields: itemInfo.additionalFields,
        author: auth.currentUser.displayName,
        createdAt: serverTimestamp(),
      });

      await onItemCreated({
        id: newItemDocRef.id,
        name: itemInfo.name,
        description: itemInfo.description,
        tags: itemInfo.tags,
        additionalFields: itemInfo.additionalFields,
        author: auth.currentUser.displayName,
      });

      toast.success('Item created successfully!');
      setItemInfo({
        name: '',
        description: '',
        tags: [],
        inputValue: '',
        additionalFields: [],
        newFieldName: '',
        newFieldType: '',
      });
    } catch (error) {
      console.error(toast.error('Error creating item: ', error));
    }
  };

  return (
    <div className='section'>
      {auth.currentUser ? (
        <div>
          <input
            required
            placeholder='Item Name:'
            className="input"
            type="text"
            value={itemInfo.name}
            onChange={(e) => setItemInfo((prevInfo) => ({ ...prevInfo, name: e.target.value }))}
          />
          <textarea
            required
            placeholder='Item Description:'
            className="textarea mt-1"
            value={itemInfo.description}
            onChange={(e) => setItemInfo((prevInfo) => ({ ...prevInfo, description: e.target.value }))}
          />
          <div style={{ display: 'flex', flexWrap: 'wrap', borderRadius: '5px' }}>
            {itemInfo.tags.map((tag, index) => (
              <div key={index} className="tag is-black" style={{ padding: '5px 10px', borderRadius: '3px', margin: '3px', cursor: 'pointer' }} onClick={() => removeTag(index, itemInfo, setItemInfo)}>
                {tag} &nbsp;<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAUUlEQVR4nJWQ0QnAIAwFJV3DWesq9csxrzyIEESIHgTk5STRUm4AKtAAC5l5VqOoQHQXzM+iRTE2Pi/mxXW85CmIATy7PXPxeHTymPf+e074AX+6wex8quSoAAAAAElFTkSuQmCC"></img>
              </div>
            ))}
            <input
              required
              className='input mt-1 mb-1'
              type="text"
              placeholder="Add Tag"
              value={itemInfo.inputValue}
              onChange={(e) => setItemInfo((prevInfo) => ({ ...prevInfo, inputValue: e.target.value }))}
              onKeyDown={(e) => handleKeyDown(e, itemInfo, setItemInfo)}
              style={{ flex: '1', outline: 'none', fontSize: '16px', fontFamily: 'Arial' }}
            />
          </div>

          <label className='subtitle'>Additional Fields:</label>
          <div>
            {itemInfo.additionalFields.map((field, index) => (
              <div key={index}>
                <input
                  required
                  type="text"
                  className='m-2'
                  placeholder="Field Name"
                  value={field.name}
                  onChange={(e) => updateFieldName(index, e.target.value, itemInfo, setItemInfo)}
                />
                <input
                  required
                  type={field.type === 'Boolean' ? 'checkbox' : 'text'}
                  className='mr-2'
                  checked={field.value}
                  onChange={(e) => updateFieldValue(index, e.target.value, itemInfo, setItemInfo)}
                />
                <button className='button is-small is-danger' onClick={() => removeField(index, itemInfo, setItemInfo)}>Remove</button>
              </div>
            ))}
          </div>
          <input
            required
            className='input'
            type="text"
            placeholder="New Field Name"
            value={itemInfo.newFieldName}
            onChange={(e) => setItemInfo((prevInfo) => ({ ...prevInfo, newFieldName: e.target.value }))}
          />
          <p className='has-text-grey-light is-size-6	p-3'>**Please fill in the Filed Name first !</p>
          <select className='button is-small mr-1' value={itemInfo.newFieldType} onChange={(e) => setItemInfo((prevInfo) => ({ ...prevInfo, newFieldType: e.target.value }))}>
            <option value="">Select Type</option>
            <option value="Text">Text</option>
            <option value="Number">Number</option>
            <option value="Boolean">Boolean</option>
          </select>
          <button className='button is-small is-warning' onClick={() => addField(itemInfo, setItemInfo)}>Add Field</button>

          <br />
          <button className="button mt-3" onClick={createItem}>
            Create Item
          </button>
          <Toaster />
        </div>
      ) : (
        <p>
          Please <Link to="/signin">sign in</Link> to create an item.
        </p>
      )}
    </div>
  );
};

export default CreateItem;
