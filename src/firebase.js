// Import the functions you need from the SDKs you need
import { initializeApp} from "firebase/app";
import { getAuth, GoogleAuthProvider, TwitterAuthProvider } from "firebase/auth";
import {getFirestore} from 'firebase/firestore';

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyB5V-fBG0580rcZfHL1uvUTTiyxdvs8amc",
  authDomain: "fir-auth-a3cbd.firebaseapp.com",
  projectId: "fir-auth-a3cbd",
  storageBucket: "fir-auth-a3cbd.appspot.com",
  messagingSenderId: "280112716162",
  appId: "1:280112716162:web:a2e70a4eeb7b4d11c2179b"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Firebase Authentication and get a reference to the service
export const auth = getAuth(app);
export const twitterProvider = new TwitterAuthProvider();
export const googleProvider = new GoogleAuthProvider();
export const db = getFirestore(app);
