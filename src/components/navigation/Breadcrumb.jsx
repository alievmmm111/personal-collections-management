import React from 'react';
import { Link, useNavigate } from 'react-router-dom';

const Breadcrumb = () => {
  const navigate = useNavigate();

  const goBack = () => {
    navigate(-1);
  };

  return (
    <nav className="breadcrumb is-right" aria-label="breadcrumbs">
      <ul>
        <li>
          <Link to="#" onClick={goBack}>
            Go Back
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default Breadcrumb;
