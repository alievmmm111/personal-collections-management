import 'bulma/css/bulma.min.css'
import './index.css'
import { BrowserRouter as Router, Routes, Route, Navigate } from "react-router-dom";
import { SignIn } from './components/auth/SignIn';
import { SignUp } from './components/auth/SignUp';
import { Home } from './components/pages/Home';
import Navbar from './components/navigation/Navbar';
import { UserProfile } from './components/pages/UserProfile';
import Collection from './components/collections/Collection';
import NotFound from './components/NotFound';
import CreateCollection from './components/collections/CreateCollection';
import ItemDetail from './components/collections/items/ItemDetail';

function App() {
  return (
    <div className="App">
      <Router>
      <Navbar/>
      <div className="container">
        <Routes>
          <Route path='/signin' element={<SignIn/>} />
          <Route path='/signUp' element={<SignUp/>} />
          <Route exact path='/' element={<Home/>}/>
          <Route path='/create-collection' element={<CreateCollection/>} />
          <Route path="/collections/:collectionId" element={<Collection/>} />
          <Route path="/collections/:collectionId/items/:id" element={<ItemDetail />} />
          <Route element={<NotFound/>} />
          <Route path='/user-details' element={<UserProfile/>} />
        </Routes>
        </div>
      </Router>
    </div>
  );
}

export default App;
