import toast from 'react-hot-toast';

export const handleKeyDown = (e, itemInfo, setItemInfo) => {
  if (e.key === 'Enter' && itemInfo.inputValue.trim() !== '') {
    setItemInfo((prevInfo) => ({
      ...prevInfo,
      tags: [...prevInfo.tags, prevInfo.inputValue.trim()],
      inputValue: '',
    }));
  }
};

export const removeTag = (index, itemInfo, setItemInfo) => {
  setItemInfo((prevInfo) => {
    const updatedTags = [...prevInfo.tags];
    updatedTags.splice(index, 1);
    return { ...prevInfo, tags: updatedTags };
  });
};

export const addField = (itemInfo, setItemInfo) => {
  if (!itemInfo.newFieldName || !itemInfo.newFieldType) {
    console.error('Please enter a name and select a type for the new field.');
    return;
  }

  setItemInfo((prevInfo) => ({
    ...prevInfo,
    additionalFields: [...prevInfo.additionalFields, { type: prevInfo.newFieldType, value: '', name: prevInfo.newFieldName }],
    newFieldName: '',
    newFieldType: '',
  }));
};

export const removeField = (index, itemInfo, setItemInfo) => {
  setItemInfo((prevInfo) => {
    const updatedFields = [...prevInfo.additionalFields];
    updatedFields.splice(index, 1);
    return { ...prevInfo, additionalFields: updatedFields };
  });
};

export const updateFieldValue = (index, value, itemInfo, setItemInfo) => {
  setItemInfo((prevInfo) => {
    const updatedFields = [...prevInfo.additionalFields];
    updatedFields[index].value = value;
    return { ...prevInfo, additionalFields: updatedFields };
  });
};

export const updateFieldName = (index, name, itemInfo, setItemInfo) => {
  setItemInfo((prevInfo) => {
    const updatedFields = [...prevInfo.additionalFields];
    updatedFields[index].name = name;
    return { ...prevInfo, additionalFields: updatedFields };
  });
};