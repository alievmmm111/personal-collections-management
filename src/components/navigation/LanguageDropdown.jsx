import React, {useState, useEffect} from 'react';
import { useTranslation } from 'react-i18next';
import langIcon from '../../static/world-wide-web.png' 

const LanguageDropdown = () => {
    const [isOpen, setIsOpen] = useState(false);
    const [selectedLanguage, setSelectedLanguage] = useState('');
    const {i18n} = useTranslation("global");

    const handleChangeLanguage = (lang) => {
      i18n.changeLanguage(lang);
      setIsOpen(false);
    }

    const toggleDropdown = () => {
      setIsOpen(!isOpen);
    };

    useEffect(() => {
      const language = i18n.language === 'en' ? 'English' : 'Русский';
      localStorage.setItem('selectedLanguage', language);
      setSelectedLanguage(language);
    }, [i18n.language]);

  
    return (
        <div>
            <div className={`dropdown ${isOpen ? 'is-active' : ''}`}>
      <div className="dropdown-trigger mr-1">
        <button className="button is-small " aria-haspopup="true" aria-controls="dropdown-menu" onClick={toggleDropdown}>
        <img src={langIcon} alt="" />
            &nbsp;{selectedLanguage}
        </button>
      </div>

      <div className="dropdown-menu" id="dropdown-menu" role="menu">
        <div className="dropdown-content">
  
          <button className="button dropdown-item is-white" onClick={() => handleChangeLanguage("en")}>
            English &nbsp;<span className='has-text-grey-light is-size-7'>(English American)</span>
          </button>
    
          <button className="button dropdown-item is-white" onClick={() => handleChangeLanguage("ru")}>
            Русский &nbsp;<span className='has-text-grey-light is-size-7'>(Russian)</span>
          </button>
         
        </div>
      </div>
    </div>
        </div>
    );
}

export default LanguageDropdown;
