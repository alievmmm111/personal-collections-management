import React, { useState} from 'react';
import { Link} from "react-router-dom";
import LanguageDropdown from './LanguageDropdown.jsx';
import UserAuthNavPart from './UserAuthNavPart.jsx';
import { useTranslation } from 'react-i18next';
import DarkMode from '../dark-theme/DarkMode.jsx';

const Navbar = () => {
  const [isActive, setIsActive] = useState(false);
  const {t} = useTranslation("global");

  const toggleMenu = () => {
    setIsActive(!isActive);
  };

    return (
        <nav className="navbar is-fixed-top">
      <div className="navbar-brand">
        <Link className="navbar-item is-uppercase has-text-weight-semibold logo" to='/'>
        Personal Collections Manager
        </Link>
        <div
          className={`navbar-burger ${isActive ? 'is-active' : ''}`}
          onClick={toggleMenu}
          aria-label="menu"
          aria-expanded="false"
          data-target="navbarExampleTransparentExample"
        >
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>

      <div id="navbarExampleTransparentExample" className={`navbar-menu ${isActive ? 'is-active' : ''}`}>
        <div className="navbar-start">
          <Link className="navbar-item" to="/">
          {t("navbar.link")}
          </Link>
        </div>

        <div className="navbar-end ">
          <div className="navbar-item">
          <DarkMode/>
          </div>
          <div className="navbar-item">
            <div className="field is-grouped">
              <LanguageDropdown/>
            <div>
              
            <UserAuthNavPart/>
        </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
    );
}

export default Navbar;
