import React, { useState, useEffect } from 'react';
import { getDocs, collection } from 'firebase/firestore';
import { db } from '../../firebase';
import CollectionItem from './CollectionItem';


const CollectionsList = () => {
  const [collections, setCollections] = useState([]);

  useEffect(() => {
    const fetchCollections = async () => {
      try {
        const collectionsSnapshot = await getDocs(collection(db, 'collections'));
        const collectionsData = collectionsSnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
        setCollections(collectionsData);
      } catch (error) {
        console.error('Error fetching collections: ', error);
      }
    };

    fetchCollections();
  }, []);

  return (
    <div className='card-collections'>
      {collections.map(collection => (
        <CollectionItem key={collection.id} collection={collection} />
      ))}
    </div>
  );
};

export default CollectionsList;
